# techjobs.tokyo.JP Frontend

The frontend for techjobs.tokyo.

This project expects to interact with a [Job Board API](https://gitlab.com/mrman/job-board-api).

# Dependencies & related tech

1. [Node](https://nodejs.org)
2. [NPM](https://npmjs.com)
3. [Vue.js](https://vuejs.org)
4. [JSPM](https://jspm.io)

# Getting started #

## Build ##

1. `npm install`
2. `./node_modules/.bin/jspm registry create gitlab jspm-git` (base URL is `ssh://git@gitlab.com`)

# How to Use #

This frontend is meant to be hosted alongside a [Job Board API](https://gitlab.com/mrman/job-board-api) instance, likely along with a [Job Board Web Admin](https://gitlab.com/mrman/job-board-admin-web) instance.

The API instance is expected to be accessible from `//api/...` on the same domain -- this is where the job board will make requests to in order to find information.
