import Vue from "app/vue";
import Util from "mrman/vue-component-library/util";
import Constants from "app/constants";

import Job from "app/models/job";
import JobPostingRequest from "app/models/job-posting-request";
import JobWithCompany from "app/models/job-with-company";

var JobsService = new Vue({
  data: {},

  methods: {

    /**
     * Add a job posting request
     *
     * @param {object} request - Request that represents the job posting to be created
     * @returns A promise that resolves to the created job posting request
     */
    addPostingRequest: function(req) {
      return Util.postToAPI(`${Constants.URLS.API_BASE_URL}/${Constants.URLS.JOBS_POSTINGS_REQUESTS}`, req)
        .then(Util.expectEnveloped200ReturningModel(JobPostingRequest));
    },

    /**
     * Get a job posting request by ID
     *
     * @param {number} id - ID of the job posting request
     * @returns A promise that resolves to the job posting request returned from the backend
     */
    fetchJobPostingRequestById: function(id) {
      return Util.getFromAPI(`${Constants.URLS.API_BASE_URL}/${Constants.URLS.JOBS_POSTINGS_REQUESTS}/${id}`)
        .then(Util.expectEnveloped200ReturningModel(JobPostingRequest));
    },

    /**
     * Get a job by ID
     *
     * @param {number} id - ID of the job posting request
     * @returns A promise that resolves to the job returned from the backend
     */
    fetchJobById: function(id) {
      return Util.getFromAPI(`${Constants.URLS.API_BASE_URL}/${Constants.URLS.JOBS}/${id}`)
        .then(Util.expectEnveloped200ReturningModel(Job));
    },

    /**
     * Fetch favorited jobs for current user
     *
     * @param {number} userId - Id of the user for which to get favorites for. If not provided, current user is assumed (/me).
     * @returns A promise that resolves to jobs that were favorited by the user
     */
    fetchFavoriteJobsForUser: function(id) {
      return Util.getFromAPI(`${Constants.URLS.API_BASE_URL}/${Constants.URLS.USERS}/${id}/${Constants.URLS.STARRED_JOBS}`)
        .then(Util.expectEnveloped200ReturningListOfModel(JobWithCompany)); // This request doesn't give a paginated list??
    },

    /**
     * Approve a job posting request by ID
     *
     * @param {number} id - ID of the job posting request
     * @returns A promise that resolves to the job that was approved
     */
    approveJobPostingRequestById: function(id) {
      if (!id) { return Promise.reject(new Error("Missing posting request ID")); }

      return Util.getFromAPI(`${Constants.URLS.API_BASE_URL}/${Constants.URLS.JOBS_POSTINGS_REQUESTS}/${id}/approve`)
        .then(Util.expectEnveloped200ReturningModel(Job));
    },

    /**
     * Update a job posting request
     *
     * @param {number} id - ID of the job posting request
     * @param {Object} update - A chunk of the update to the company to make, expected to be a merge patch ({field: value}), see PATCH spec
     * @returns A promise that resolves to teh job posting request that was updated
     */
    updateJobPostingRequestById: function(id, update) {
      if (!id || !update) { return Promise.reject(new Error("Both posting request ID and company ID must be provided")); }

      return Util.mergePatchToAPI(`${Constants.URLS.API_BASE_URL}/${Constants.URLS.JOBS_POSTINGS_REQUESTS}/${id}`, update)
        .then(Util.expectEnveloped200ReturningModel(JobPostingRequest));
    },

    /**
     * Search for jobs WITHOUT requirement that they be active (for adminstrators)
     *
     * @param {Object} query - A query parsable by the backend
     * @returns A promise that resolves to the list of jobs that match the given query
     */
    search: function(query) {
      var pathWithQuery = Constants.URLS.JOBS_SEARCH + Util.makeJobSearchQueryString(query);
      return Util.getFromAPI(`${Constants.URLS.API_BASE_URL}/${pathWithQuery}`)
        .then(Util.expectEnveloped200ReturningModel(JobWithCompany, {paginated: true}));
    },

    /**
     * Enable/disable a job by ID
     *
     * @param {number} id - ID of the job
     * @param {boolean} shouldBeActive - Represents whether the job should be changed to be active/inactive
     * @returns A promise that resolves to the job
     */
    changeJobActiveStatusByID: function(id, shouldBeActive) {
      return Util.postToAPI(`${Constants.URLS.API_BASE_URL}/${Constants.URLS.JOBS}/${id}/${shouldBeActive ? "activate" : "deactivate"}`)
        .then(Util.expectEnveloped200ReturningModel(Job))
        .then(updatedJob => {
          this.$emit("job-changed", updatedJob);
          return updatedJob;
        });
    },

    /**
     * Enable/disable a job by ID, for a particular company
     *
     * @param {number} cid - ID of the company
     * @param {number} jid - ID of the job
     * @param {boolean} shouldBeActive - Represents whether the job should be changed to be active/inactive
     * @returns A promise that resolves to the job
     */
    changeJobActiveStatusByIDForCompany: function(cid, jid, shouldBeActive) {
      return Util.postToAPI(`${Constants.URLS.API_BASE_URL}/${Constants.URLS.COMPANIES}/${cid}/jobs/${jid}/${shouldBeActive ? "activate" : "deactivate"}`)
        .then(Util.expectEnveloped200ReturningModel(Job))
        .then(updatedJob => {
          this.$emit("job-changed", updatedJob);
          return updatedJob;
        });
    },

    /**
     * Update job by ID
     *
     * @param {number} id - ID of the job
     * @param {Job} job - Updated job
     * @returns A promise that resolves to the updated job
     */
    updateJobByID: function(id, job) {
      return Util.postToAPI(`${Constants.URLS.API_BASE_URL}/${Constants.URLS.JOBS}/${id}`, job.serialize())
        .then(Util.expectEnveloped200ReturningModel(Job))
        .then(updatedJob => {
          this.$emit("job-changed", updatedJob);
          return updatedJob;
        });
    }

  }
});

export default JobsService;
