import _ from "lodash";
import Vue from "app/vue";
import Constants from "app/constants";
import Util from "mrman/vue-component-library/util";

import StateService from "app/services/state";
import CompaniesService from "app/services/companies";

import User from "app/models/user";

const LS_USER_SERVICE_STATE = "state-user-service";
const DEFAULT_STATE = {
  session: null,
  userIsLoggedIn: false,
  starredJobsLookup: {},
  companiesRepresentedByCurrentUser: [],
  queriedOnce: false
};

var UserService = new Vue({
  data: function() {
    var state = {};

    var throttledFetchAndUpdateStarredJobs = _.debounce(this.fetchAndUpdateStarredJobs, 500, {leading: true, trailing: false});

    try {
      state = JSON.parse(StateService.getKVState(LS_USER_SERVICE_STATE));
    } finally {
      state = _.merge(DEFAULT_STATE, state);
    }

    return {
      state,
      throttledFetchAndUpdateStarredJobs: throttledFetchAndUpdateStarredJobs
    };
  },

  created: function() {
    // Check if the user is logged in
    this.hasValidSession();
  },

  computed: {
    currentUser: function() { return this.state.session ? new User(this.state.session.sessionUserInfo) : null; },
    currentUserEmail: function() { return this.currentUser ? this.currentUser.email : ""; },
    currentUserId: function() { return this.currentUser ? this.currentUser.id : ""; },

    isAdmin: function() { return this.currentUser ? this.currentUser.role === Constants.ROLES.ADMINISTRATOR : false; },
    isJobSeeker: function() { return this.currentUser ? this.currentUser.role === Constants.ROLES.JOB_SEEKER : false ; },
    isCompanyRepresentative: function() { return this.currentUser ? this.currentUser.role === Constants.ROLES.COMPANY_REPRESENTATIVE : false ; },

    userPermissions: function() { return this.state.session ? this.state.session.sessionUserPermissions : null; }
  },

  methods: {

    /**
     * Save local state -- possibly to local storage or other client-side persistence.
     */
    saveState: function() {
      StateService.setKVState(LS_USER_SERVICE_STATE, JSON.stringify(this.state));
    },

    /**
     * Return whether the user service has a valid session, by checking with the backend
     *
     * @returns a boolean that represents whether the session is valid
     */
    hasValidSession: function() {
      return this.fetchCurrentUser()
        .then(exists => exists === null ? false : true)
        .catch(() => false);
    },

    /**
     * Set the current session info
     *
     * @param {Object} sessionUserInfo - An object with information about the user session
     * @param {string} sessionUserInfo.expires - The date (milliseconds since epoch) when the session expires
     * @returns A promise that resolves to the value of the API key
     */
    setSession: function(s) {

      return new Promise((resolve, reject) => {
        // Ensure required keys are present, and key is not undefined or empty
        if (!Util.isValidSession(s)) {
          reject(new Error("Invalid session"));
          return;
        }

        // Ensure date is JS date
        if (!_.isDate(s.expires)) { s.expires = new Date(s.expires); }

        // Set and save to localstorage
        this.state.session = s;
        this.state.userIsLoggedIn = true;

        if (this.isCompanyRepresentative) {
          this.getOrFetchCompaniesRepresentedByCurrentUser();
        }

        this.saveState();
        resolve(this.state.session);
      });
    },


    getSession: function() { return this.state.session; },

    resetSession: function(e) {
      this.state.session = null;
      this.state.companiesRepresentedByCurrentUser = [];
      this.state.userIsLoggedIn = false;
      this.state.starredJobsLookup = {};
      this.state.queriedOnce = false;

      this.saveState();

      // Rethrow error if provided
      if (!Util.isNullOrUndefined(e) && _.isError(e)) { throw e; }
    },

    getOrFetchCompaniesRepresentedByCurrentUser: function() {
      var cache = this.state.companiesRepresentedByCurrentUser;
      return _.isEmpty(cache) ? this.fetchCompaniesRepresentedByCurrentUser() : Promise.resolve(cache);
    },

    fetchCompaniesRepresentedByCurrentUser: function() {
      // Get the companies represented by the current user
      CompaniesService
        .fetchCompaniesRepresentedByUser(this.currentUser.id)
        .then(_.partial(Util.addDifferences, this.state.companiesRepresentedByCurrentUser));
    },

    fetchCurrentUser: function() {
      return Util.getFromAPI(`${Constants.URLS.API_BASE_URL}/${Constants.URLS.ME}`)
        .then(Util.errorOnStatusOtherThan([200]))
        .then(Util.jsonifyFetchResponse)
        .then(_.partialRight(_.get, "respData"))
        .then(this.setSession)
        .catch(e => {
          this.resetSession(e);
          throw e;
        });
    },

    /**
     * Attempt to sign up a new user
     *
     * @param {User} userReq - Partially-filled new user object
     * @param {string} userReq.email - new user's email,
     * @param {string} userReq.firstName - new user's firstName,
     * @param {string} userReq.lastName - new user's lastName,
     * @param {string} userReq.password - new user's password,
     * @param {string} userReq.role - new user's role,
     * @param {string} userReq.joinedAt - new user's Date()
     * @returns a Promise that resolves to the response submitted by the data
     */
    signup: function(userReq) {
      if (!userReq) {
        return Promise.reject(new Error("Invalid/missing new user request object"));
      }

      return Util.postToAPI(`${Constants.URLS.API_BASE_URL}/${Constants.URLS.USERS}`, userReq)
        .then(Util.jsonifyFetchResponse);
    },

    /**
     * Attempt to login with a given email and password
     *
     * @param {string} email
     * @param {string} password
     * @returns a Promise
     */
    login: function(email, password) {
      // Ensure email/password are provided
      if (_.isUndefined(email) || !_.isString(email) || _.isEmpty(email.trim())) {
        return Promise.reject(new Error("Invalid user email"));
      }

      if (_.isUndefined(password) || !_.isString(password) || _.isEmpty(password.trim())) {
        return Promise.reject(new Error("Invalid user password"));
      }

      // Trim email & pass
      email = email.trim();
      password = password.trim();
      var creds = {userEmail: email, userPassword: password};

      return Util.postToAPI(`${Constants.URLS.API_BASE_URL}/${Constants.URLS.LOGIN}`, creds)
        .then(Util.jsonifyFetchResponse)
        .then(_.partialRight(_.get, "respData"))
        .then(this.setSession);
    },

    // Send logout to backend
    sendLogout: function() {
      return Util.getFromAPI(`${Constants.URLS.API_BASE_URL}/${Constants.URLS.LOGOUT}`)
        .then(Util.errorOnStatusOtherThan([200]))
        .then(Util.jsonifyFetchResponse)
        .then(() => this.resetSession());
    },

    // Log out the current user
    logout: function() {
      var currentSession = this.getSession();

      return this.sendLogout()
        .then(() => this.resetSession())
        .then(() => currentSession);
    },

    /**
     * Change password for current user
     *
     * @param {PasswordChangeRequest} req - The password change request, containing the old and new password (twice)
     * @returns A promise that resolves to whether the change was made on the server (boolean)
     */
    changePassword: function(req) {
      return Util.postToAPI(`${Constants.URLS.API_BASE_URL}/${Constants.URLS.ME}/${Constants.URLS.CHANGE_PASSWORD}`, req.serialize())
        .then(Util.errorOnStatusOtherThan([200]))
        .then(Util.jsonifyFetchResponse);
    },

    /**
     * Check if a job is starred for the given user. Depending on state this may perform a fetch
     *
     * @param {number} jobId - ID of the job for which to check
     * @returns A promise that resolves to whether the job has been starred
     */
    jobIsStarred: function(jid) {
      var shouldFetch = _.keys(this.state.starredJobsLookup).length === 0 && !this.state.queriedOnce;
      return (shouldFetch ? this.throttledFetchAndUpdateStarredJobs() : Promise.resolve(this.state.starredJobsLookup))
        .then(lookup => _.get(lookup, jid, false) )
        .catch(() => false);
    },

    fetchAndUpdateStarredJobs: function() {
      return Util.getFromAPI(`${Constants.URLS.API_BASE_URL}/${Constants.URLS.ME}/${Constants.URLS.STARRED_JOBS}`)
        .then(Util.errorOnStatusOtherThan([200]))
        .then(Util.jsonifyFetchResponse)
        .then(_.partialRight(_.get, "respData"))
        .then(({items: jobsWithCompanies})  => {
          var jobs = jobsWithCompanies.map(jwc => jwc.job);
          this.queriedOnce = true; // At least queried once
          this.state.starredJobsLookup = _.keyBy(jobs, j => j.id);
          this.$emit("updated-starred-jobs", this.state.starredJobsLookup);
          return this.state.starredJobsLookup;
        });
    },

    /**
     * Add a star for a job
     *
     * @param {number} jobId - The job which will be starred by the current user
     * @returns A promise that evaluates when the job has been starred
     */
    starJob: function(jobId) {
      return Util.postToAPI(`${Constants.URLS.API_BASE_URL}/${Constants.URLS.ME}/${Constants.URLS.STARRED_JOBS}/${jobId}`)
        .then(Util.errorOnStatusOtherThan([200]))
        .then(Util.jsonifyFetchResponse)
        .then(_.partialRight(_.get, "respData"))
        .then(updatedCount => {
          if (updatedCount === 1) {
            this.state.starredJobsLookup[jobId] = true;
            this.$emit("starred-job", jobId);
          }
        });
    },

    /**
     * Add a star for a job
     *
     * @param {number} jobId - The job which will be starred by the current user
     * @returns A promise that evaluates when the job has been starred
     */
    unStarJob: function(jobId) {
      return Util.deleteFromAPI(`${Constants.URLS.API_BASE_URL}/${Constants.URLS.ME}/${Constants.URLS.STARRED_JOBS}/${jobId}`)
        .then(Util.errorOnStatusOtherThan([200]))
        .then(Util.jsonifyFetchResponse)
        .then(_.partialRight(_.get, "respData"))
        .then(updatedCount => {
          if (updatedCount === 1) {
            this.state.starredJobsLookup[jobId] = false;
            this.$emit("unstarred-job", jobId);
          }
        });
    }


  }
});

export default UserService;
