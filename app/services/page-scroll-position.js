import Vue from "app/vue";


var PageScrollPositionService = new Vue({
  data: {
    scrollWatchUnbindFn: () => {}
  },

  created: function() {
    this.scrollWatchUnbindFn = window.document.addEventListener("scroll", e => {
      this.$emit("scroll", e);
    });
  },

  beforeDestroy: function() {
    this.scrollWatchUnbindFn();
  }

});

export default PageScrollPositionService;
