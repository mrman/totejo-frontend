import Vue from "app/vue";
import _ from "lodash";
import Util from "mrman/vue-component-library/util";
import Constants from "app/constants";

import Company from "app/models/company";
import Job from "app/models/job";

/**
 * Holds/fetches companies from the backend (with a cache of them if fetching isn't needed)
 */
var CompaniesService = new Vue({

  data: function() {
    return {
      state: {
        companiesById: {}
      }
    };
  },

  methods: {

    /**
     * Get or fetch all companies
     *
     * @param {object} opts - Options
     * @param {boolean} opts.forceFetch
     * @returns A promise that resolves to all the companies in this service
     */
    getOrFetchAllCompanies: function(opts={}) {
      if (opts.forceFetch) { return this.fetchCompanies(); }

      // In the (hopefully) unlikely event that there are no companies in the cache, fetch all from backend
      return _.isEmpty(this.state.companiesById) ? this.fetchCompanies() : Promise.resolve(_.values(this.state.companiesById));
    },

    /**
     * Extract and cache the company from the result of a fetch
     *
     * @param {Object} info - backend envelope containing company and permissions
     * @param {CompanyInfo} info.company - company object
     * @param {string} info.company.companyID - ID of the company object
     * @param {Object[]} permission - permissions for the retrieved company
     * @returns The `info` object
     */
    cacheAndReturnCompany: function(c) {
      // Attempt to find/add a company with the ID already present
      if (!_.has(this.state.companiesById, c.id)) {
        this.state.companiesById[c.id] = c;
      }

      return c;
    },

    /**
     * Get or fetch a company by ID
     *
     * @param {string|number} id - the ID to use to search for the company
     * @returns A promise that resolves to either the fetched or cached company model
     */
    getOrFetchCompanyById: function(companyId) {
      companyId = _.isString(companyId) ? parseInt(companyId) : companyId;
      if (!_.isNumber(companyId)) {
        return Promise.reject(new Error("Invalid input, companyId should be number"));
      }

      return companyId in this.state.companiesById ? Promise.resolve(this.state.companiesById[companyId]) : this.fetchCompanyById(companyId);
    },

    /**
     * Get company information from backend
     *
     * @param {number} id - The ID of the company to retrieve
     * @returns A promise that resolves to an object that looks like {respData: Company}
     */
    fetchCompanyById: function(id) {
      if (Util.isNullOrUndefined(id)) {
        return Promise.reject(new Error("Invalid company ID: " + id));
      }

      return Util.getFromAPI(`${Constants.URLS.API_BASE_URL}/${Constants.URLS.COMPANIES}/${id}`)
        .then(Util.expectEnveloped200ReturningModel(Company))
        .then(this.cacheAndReturnCompany);
    },

    /**
     * Fetch all companies from the backend
     *
     * @returns A promise that resolves to all companies available on the backend
     */
    fetchCompanies:  function() {
      return Util.getFromAPI(`${Constants.URLS.API_BASE_URL}/${Constants.URLS.COMPANIES}`)
        .then(Util.expectEnveloped200ReturningListOfModel(Company))
        .then(companies => companies.map(this.cacheAndReturnCompany));
    },

    /**
     * Fetch companies that are represented by the given user (who should be a user with CompanyRepresentative role)
     *
     * @param {number} id - ID of the user for which to pull all represented companies
     *
     */
    fetchCompaniesRepresentedByUser: function(id) {
      return Util.getFromAPI(`${Constants.URLS.API_BASE_URL}/${Constants.URLS.COMPANIES}/represented-by/${id}`)
        .then(Util.expectEnveloped200ReturningListOfModel(Company));
    },

    /**
     * Create a new company on the backend
     *
     * @param {Company} c - The company to attempt to create
     * @returns A promise that resolve to the created object
     */
    createNewCompany: function(c) {
      return Util.postToAPI(`${Constants.URLS.API_BASE_URL}/${Constants.URLS.COMPANIES}`, c.serialize())
        .then(Util.expectEnveloped200ReturningModel(Company))
        .then(c => {
          this.$emit("new-company-created", c);
          return c;
        });
    },

    /**
     * Add a representative for a given company
     *
     * @param {Number} cid - ID of the company
     * @param {String} userEmail - Email of the user who will become the company's representative
     * @returns A promise that resolves to whether the addition succeeded or failed
     */
    addRepresentativeForCompany: function(cid, userEmail) {
      return Util.postToAPI(`${Constants.URLS.API_BASE_URL}/${Constants.URLS.COMPANIES}/${cid}/representatives/${userEmail}`)
        .then(Util.errorOnStatusOtherThan([200]))
        .then(Util.jsonifyFetchResponse)
        .then(({respData}) => respData);
    },

    /**
     * Remove a representative for a given company
     *
     * @param {Number} cid - ID of the company
     * @param {String} userEmail - Email of the user who will become the company's representative
     * @returns A promise that resolves to whether the remove succeeded or failed
     */
    removeRepresentativeForCompany: function(cid, userEmail) {
      return Util.deleteFromAPI(`${Constants.URLS.API_BASE_URL}/${Constants.URLS.COMPANIES}/${cid}/representatives/${userEmail}`)
        .then(Util.errorOnStatusOtherThan([200]))
        .then(Util.jsonifyFetchResponse)
        .then(({respData}) => respData);
    },

    /**
     * Fetch statistics for a given company
     *
     * @param {Number|String} cid - ID of the company
     * @returns A promise that resolves to the statistics for a given company
     */
    fetchStatisticsForCompany: function(cid) {
      return Util.getFromAPI(`${Constants.URLS.API_BASE_URL}/${Constants.URLS.COMPANIES}/${cid}/statistics`)
        .then(Util.errorOnStatusOtherThan([200]))
        .then(Util.jsonifyFetchResponse)
        .then(({respData}) => respData);
    },

    /**
     * Fetch jobs for a given company (assumes non-paginated backend endpoint)
     *
     * @param {Number|String} cid - ID of the company
     * @returns A promise that resolves to all jobs for a given company
     */
    fetchJobsForCompany: function(cid) {
      return Util.getFromAPI(`${Constants.URLS.API_BASE_URL}/${Constants.URLS.COMPANIES}/${cid}/jobs`)
        .then(Util.expectEnveloped200ReturningListOfModel(Job));
    }
  }
});

export default CompaniesService;
