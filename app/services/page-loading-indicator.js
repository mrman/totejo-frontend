import Vue from "app/vue";
import _ from "lodash";

var LOADING_INDICATOR_ID = 0;
const DEFAULT_LOADING_INFO = {title: "", description: ""};

/**
 * Check if an object is a valid loading info object for use with the
 *
 * @param {Object} info - The object to be checked
 * @returns a boolean representing whether the object is valid or not
 */
const isValidLoadingInfo = (info) => _.every(["title", "description"], _.partial(_.has, info));

var PageLoadingIndicatorService = new Vue({
  data: {
    state: {
      isLoading: false,
      overwriteUntil: 0,
      loadingInfo: DEFAULT_LOADING_INFO
    }
  },

  methods: {

    /**
     * Signals that the page is loading. Only one thing is allowed to be loading at the same time.
     *
     * @param {object} info - Information that should be displayed on the loading screen
     * @param {object} info.title - What to title the loading page
     * @param {object} info.description - Why/what page is loading
     * @param {object} opts - options
     * @param {boolean} opts.overwrite - option that controls whether to overwrite the current loading screen
     */
    startLoading: function(info, opts={}) {
      return new Promise((resolve, reject) => {
        var overwrite = _.get(opts, "overwrite", false);

        // If page is already loading, fail, unless overwrite is specified
        if (this.state.isLoading && !overwrite) {
          reject(new Error("Something is already in the process of loading.."));
          return;
        }


        // Ensure loading info is valid
        if (!isValidLoadingInfo(info)) {
          reject(new Error("Invalid loading info:", info));
          return;
        }

        // Update the state
        this.state.isLoading = true;
        this.state.loadingInfo = info;
        this.state.loadingInfoId = ++LOADING_INDICATOR_ID;

        if (overwrite) { this.state.overwriteUntil = this.state.loadingInfoId; }

        resolve(this.state.loadingInfoId);
      });
    },

    /**
     * Mark a loading process as complete, resetting the service state
     *
     * @param {number} id - the loading instance ID to use
     */
    finishLoading: function(id) {
      return new Promise((resolve, reject) => {

        if (!(id == this.state.loadingInfoId) && 
            // Don't bother with this check unless ID is over overwriteUntil
            this.state.overwriteUntil < id)  {
          reject(new Error(`ID [${id}] does not match ID for currently shown loading page [${this.state.loadingInfoId}], another page/action might have started loading.`));
          return;
        }

        this.state.loadingInfo = DEFAULT_LOADING_INFO;
        this.state.isLoading = false;

        resolve(_.clone(this.state));
      });
    }

  }

});

export default PageLoadingIndicatorService;
