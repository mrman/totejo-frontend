import Vue from "app/vue";
import _ from "lodash";
import Util from "mrman/vue-component-library/util";
import Constants from "app/constants";

import Tag from "app/models/tag";

/**
 * Holds/fetches tags from the backend (with a cache of them if fetching isn't needed)
 *
 * @class TagsService
 */
var TagsService = new Vue({

  data: function() {
    return {
      state: {
        tagsByName: {} // cache of tags
      }
    };
  },

  methods: {

    /**
     * Get or fetch all tags
     *
     * @param {object} opts - Options
     * @param {boolean} opts.forceFetch
     * @returns A promise that resolves to all the tags in this service
     */
    getOrFetchAllTags: function(opts={}) {
      if (opts.forceFetch) { return this.fetchTags(); }

      // In the (hopefully) unlikely event that there are no tags in the cache, fetch all from backend
      return _.isEmpty(this.state.tagsByName) ? this.fetchTags() : Promise.resolve(_.values(this.state.tagsByName));
    },

    /**
     * Extract and cache the tag from the result of a fetch
     *
     * @param {Tag} t - the tag to cache
     * @returns The `info` object
     */
    cacheAndReturn: function(t) {
      // Attempt to find/add a company with the ID already present
      if (!_.has(this.state.tagsByName, t.name)) {
        this.state.tagsByName[t.name] = t;
      }

      return t;
    },

    /**
     * Get or fetch a tag by name
     *
     * @param {string} name - The name of the tag
     * @returns A promise that resolves to either the fetched or cached company model
     */
    getOrFetchTagByName: function(tagName) {
      if (!_.isString(tagName)) {
        return Promise.reject(new Error("Invalid input, tagName should be a string"));
      }

      return tagName in this.state.tagsByName ? Promise.resolve(this.state.tagsByName[tagName]) : this.fetchTagByName(tagName);
    },

    /**
     * Find tags that based on a given query
     *
     * @param {number} id - The ID of the tag to retrieve
     * @returns A promise that resolves to an object that looks like {respData: Tag}
     */
    searchTags: function(query) {
      if (Util.isNullOrUndefined(query)) {
        return Promise.reject(new Error("Invalid query: " + query));
      }

      return Util.getFromAPI(`${Constants.URLS.API_BASE_URL}/${Constants.URLS.TAGS}/search?searchTerm=${query}`)
        .then(Util.expectEnveloped200ReturningListOfModel(Tag))
        .then(tags => tags.map(this.cacheAndReturnTag));
    },

    /**
     * Fetch all tags from the backend
     *
     * @returns A promise that resolves to all tags available on the backend
     */
    fetchTags:  function() {
      return Util.getFromAPI(`${Constants.URLS.API_BASE_URL}/${Constants.URLS.TAGS}`)
        .then(Util.expectEnveloped200ReturningListOfModel(Tag))
        .then(tags => tags.map(this.cacheAndReturn));
    },

    /**
     * Create a new tag on the backend
     *
     * @param {Tag} c - The tag to attempt to create
     * @returns A promise that resolve to the created object
     */
    createNewTag: function(t) {
      return Util.postToAPI(`${Constants.URLS.API_BASE_URL}/${Constants.URLS.TAGS}`, t.serialize())
        .then(Util.expectEnveloped200ReturningModel(Tag))
        .then(t => {
          this.$emit("new-tag-created", t);
          return t;
        });
    }

  }
});

export default TagsService;
