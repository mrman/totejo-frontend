import _ from "lodash";
import Vue from "app/vue";
import PageLoadingIndicatorService from "app/services/page-loading-indicator";
import UserService from "app/services/user";
import AlertService from "app/services/alert";
import AppService from "app/services/app";

import t from "./template.html!vtc";

import "mrman/vue-component-library/components/alert-listing/component";
import "app/components/user-role-select/component";

const comp = {
  render: t.render,
  staticRenderFns: t.staticRenderFns,

  props: {
    userSvc: {type: Object, default: () => UserService },
    appSvc: {type: Object, default: () => AppService }
  },

  data: function() {

    return {
      signupAlerts: AlertService.getAlertsForNamespace("signup"),

      email: "",
      firstName: "",
      lastName: "",
      password: "",
      passwordAgain: "",
      role: null,

      showPlainTextPassword: false,
      submittedOnce: false
    };
  },

  beforeCreate: function() {
    // Redirect to app if user is already logged in
    var loadingId;
    PageLoadingIndicatorService.startLoading({title: "Loading registration page...", description: `Sign up to use ${this.appSvc.state.appName}`})
      .then(id => loadingId = id)
      .then(UserService.hasValidSession)
      .then(loggedIn => {
        PageLoadingIndicatorService.finishLoading(loadingId)
          .then(() => {
            if (loggedIn) { AppService.doNavigation(r => r.push("/app")); }
          });
      })
      .catch(() => PageLoadingIndicatorService.finishLoading(loadingId));
  },

  computed: {
    firstNameIsValid: function() { return this.firstName.trim().length > 0; },
    lastNameIsValid: function() { return this.lastName.trim().length > 0; },
    emailIsValid: function() { return this.email.trim().length > 0; },
    passwordIsValid: function() { return this.password.trim().length > 0; },
    passwordAgainIsValid: function() { return this.passwordAgain.trim().length > 0; },
    passwordsMatch: function() { return this.password === this.passwordAgain; },

    signupFormHasErrors: function() {
      return _.some([
        this.firstNameIsValid,
        this.lastNameIsValid,
        this.emailIsValid,
        this.passwordIsValid,
        this.passwordAgainIsValid,
        this.passwordsMatch
      ], v => !v);
    }
  },

  methods: {
    updateRole: function(r) { this.role = r; },

    // Attempt to signup with the current username & password
    signup: function() {
      if (this.signupFormHasErrors) {
        this.submittedOnce = true;
        AlertService.addAlertForNamespace({
          status: "error",
          message: "Form has errors, please ensure that all fields are filled."
        }, "signup");
        return;
      }

      var newUserReq = {
        emailAddress: this.email,
        firstName: this.firstName,
        lastName: this.lastName,
        password: this.password,
        userRole: this.role,
        joinedAt: new Date()
      };

      UserService.signup(newUserReq)
        .then(resp => {
          if (resp.status === "success") {
            AlertService.addAlertForNamespace({
              status: "success",
              message: "Successfully created new user account! You may now sign in with the given credentials!"
            }, "signup");

            // Set the API key on the user service, and re-direct to app
            setTimeout(() => {
              AppService.doNavigation(r => r.push("/login"));
              AlertService.clearAlerts();
            }, 1500); // Give the user some time to see the success message
          }
        })
        .catch(function() {
          AlertService.addAlertForNamespace({
            status: "error",
            message: "Signup failed, please ensure you filled out all required fields and try again."
          }, "signup");
        });
    }
  }
};

// Register and export component
Vue.component("signup-page", comp);
export default comp;
