import Vue from "vue";
import VueRouter from "vue-router";

// Add plugins
Vue.use(VueRouter);

var window = window || window;
if (window) { window.Vue = Vue; }
export default Vue;
