export default class UserInfo {
  constructor(data={}) {
    this.user = data.userInfoUser;
    this.permissions = data.userInfoPermissions;
  }
}
