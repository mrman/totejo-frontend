// Default error (when absolutely no options are specified to the component instance)
exports.DEFAULT_ERROR = {
  propsData: {
    alert: {
      closable: true,
      status: undefined,
      message: undefined
    },
    alertIdx: undefined
  }
};

// Fixture for a simple alert @ init (prop)
exports.CLOSABLE_ERROR = {
  propsData: {
    alert: {
      closable: true,
      status: "error",
      message: "This is a test alert"
    },
    alertIdx: 0
  }
};

// Create a single hint
exports.HINT_CREATION = {
  propsData: {
    hintCreateFn: function(h) {
      return new Promise.resolve(h);
    }
  }
};
