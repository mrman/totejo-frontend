.PHONY: all build loc-count \
	webapp served-webapp served-webapp-watch served-webapp-dist served-webapp-dist-watch \
	setup webapp-manual-deploy tag-deployed-webapp webapp-push-image-remote webapp-stop-container-remote webapp-run-image-remote \
	lint tests \
	image deploy \
	test-unit test-unit-watch test-int test-e2e test-e2e-watch \
	test-e2e-stability-check selenium-instance

all: setup build tests
build: webapp
test: test-unit test-int test-e2e

HTTP_SERVER = ./node_modules/.bin/http-serve
LOCAL_WEB_PORT = 5003 # API on 5001, Admin on 5002, Job board frontend on 5003
LOCAL_API_PORT = 5001
WEB_APP_DIST_LOCATION=dist

RELEASE_VERSION=2.10
IMAGE_NAME=totejo-frontend
REGISTRY_IMAGE_NAME=registry.gitlab.com/mrman/totejo-frontend
REGISTRY_IMAGE_NAME_WEBAPP=$(REGISTRY_IMAGE_NAME)/$(IMAGE_NAME):$(RELEASE_VERSION)

MAKEFILE_DIR:=$(strip $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST)))))
JSPM=$(MAKEFILE_DIR)/node_modules/.bin/jspm

#################
# Dev Utilities #
#################

loc-count:
	echo `find app -name "*.js" | xargs wc | tail -n 1 | awk '{print $$1}'` +	\
	`find app -name "*.html" | xargs wc | tail -n 1 | awk '{print $$1}'` | bc

dev-env-setup:
	cp ./.dev-util/git/hooks/pre-push.sh .git/hooks/pre-push
	chmod +x .git/hooks/pre-push

dev-env-teardown:
	rm .git/hooks/pre-push

######################
# Code quality/Tests #
######################

lint:
	./node_modules/.bin/eslint app test

test-unit:
	./node_modules/.bin/tape app/**/*.spec.js | ./node_modules/.bin/tap-min

test-unit-watch:
	find . -not -path "node_modules/*" \
				 -not -path "jspm_packages/*" \
				 -not -path "target/*" \
				 -type f \
				 -regex ".*\.\(js\|css\|html\)$$" \
				 | entr -rc make test-unit

test-int:
	./node_modules/.bin/tape app/**/*.spec.int.js | ./node_modules/.bin/tap-min

test-e2e:
	FRONTEND_FOLDER=$(WEB_APP_DIST_LOCATION) \
	DB_MIGRATION_FOLDER=$(SQL_MIGRATIONS_FOLDER) \
	TEST_APP_BIN_PATH=$(API_EXEC_PATH) \
	CHROMEDRIVER_BIN=$(CHROMEDRIVER) \
	./node_modules/.bin/tape web/app/**/*.spec.e2e.js

test-e2e-stability-check:
	for n in `seq 1 10` ; do \
		make test-e2e; \
		if [ "$$?" -ne "0" ] ; then\
			exit 1; \
		fi \
	done

test-e2e-watch:
	find web/ -not -path "web/node_modules/*" \
						-not -path "web/jspm_packages/*" \
						-not -path "web/target/*" \
						-type f \
						-regex ".*\.\(js\|css\|html\)$$" \
					 | entr -rc make test-unit-frontend

selenium-instance:
	CHROMEDRIVER_BIN=$(CHROMEDRIVER) \
	java -jar $(SELENIUM_JAR) -Dwebdriver.chrome.driver=$(CHROMEDRIVER_BIN)

##########
# Webapp #
##########

setup:
	mkdir -p dist && \
	npm install && \
	node .dev-util/make-jspm-gitlab-registry.js && \
	node .dev-util/make-jspm-local-registry.js && \
	$(JSPM) install

webapp:
	cp index.build.html dist/index.html && \
	cp -r static/ dist/ && \
	$(JSPM) bundle-sfx  app/main dist/bundle.js --minify

# Starts local server with no cache
served-webapp:
	$(MAKEFILE_DIR)/node_modules/.bin/http-server \
		--cors \
		-p $(LOCAL_WEB_PORT) \
		-P "http://localhost:$(LOCAL_API_PORT)" \
		-c-1

# Starts local server with no cache
served-webapp-watch:
		find .  -not -path "./node_modules/*" \
					-not -path "./jspm_packages/*" \
					-not -path "./target/*" \
					-not -path "./**/*.spec.js" \
					-not -path "./dist/*" \
					-type f \
					-regex ".*\.\(js\|css\|html\)$$" \
				 | entr -rc make webapp served-webapp

served-webapp-dist:
	cd dist && \
	$(MAKEFILE_DIR)/node_modules/.bin/http-server \
		--cors \
		-p $(LOCAL_WEB_PORT) \
		-P "http://localhost:$(LOCAL_API_PORT)" \
		-c-1

# Starts local server which serves the ready-to-distribute version of the frontend
served-webapp-dist-watch:
		find . -not -path "./node_modules/*" \
					-not -path "./jspm_packages/*" \
					-not -path "**/*.spec.js" \
					-not -path "./dist/*" \
					-type f \
					-regex ".*\.\(js\|css\|html\)$$" \
				 | entr -rc make webapp served-webapp-dist

image:
	docker build -f infra/Dockerfile -t $(REGISTRY_IMAGE_NAME_WEBAPP) .

deploy: test publish

publish:
	docker push $(REGISTRY_IMAGE_NAME_WEBAPP)

#################
# Manual deploy #
#################

webapp-manual-deploy: webapp-push-image-remote webapp-stop-container-remote webapp-run-image-remote tag-deployed-webapp

tag-deployed-webapp:
	-git push --delete origin deployed-webapp
	-git tag --delete deployed-webapp
	git tag deployed-webapp HEAD # Tag where the repo is currently, rollbacks may happen
	git push --tags

# Transfer the image to the host
webapp-push-image-remote:
	@echo -e "Deploying container [$(IMAGE_NAME_WEBAPP)] to host [$(SSH_ADDR)]"
	docker save $(IMAGE_NAME_WEBAPP) | bzip2 | pv | ssh $(SSH_ADDR) 'bunzip2 | docker load'

webapp-stop-container-remote:
	ssh $(SSH_ADDR) "docker stop $(CONTAINER_NAME_WEBAPP); \
		docker rm $(CONTAINER_NAME_WEBAPP);"

# Deploy image on the host by getting teh docker host ip then using it in the run command
webapp-run-image-remote:
	@echo -e "Connecting to host host [$(SSH_ADDR)] and starting container $(CONTAINER_NAME_WEBAPP)..."
	ssh $(SSH_ADDR) "docker run \
		--name $(CONTAINER_NAME_WEBAPP) \
		-p 127.0.0.1:5000:80 \
		-d $(IMAGE_NAME_WEBAPP)"
